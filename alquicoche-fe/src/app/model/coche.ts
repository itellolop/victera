export class Coche {
    id: number;
    oficina: string;
    marca: string;
    modelo: string;
    gama: string;
    cambioManual: boolean;
    numeroPuertas: number;
    techoSolar: boolean;
    extras: string;
    disponible: boolean;

}
