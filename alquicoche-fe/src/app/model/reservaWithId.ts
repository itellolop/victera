export class ReservaWithId {
    idReserva:number;
    tarjetaCredito:string;
    matricula: string;
    email: string;
    fechaReserva: string;
    horaReserva: string;
    oficinaReserva:string;
    fechaDestino: string;
    horaDestino: string;
    oficinaDestino:string;
    estadoReserva:number;
    costeReserva:number;
    tarifaCliente:number;
    tarifaCoche:number;
    periodoAno:number;
    tipoCliente:number;
    extrasCoche: number;

    constructor(idReserva, tarjeta, matricula, email, fechaRes, horaRes, ofiRes, fechaDes, horaDes,
        ofiDes, estado, coste, tarifaCl, tarifaCo, periodo, tipoCliente, extras){
            this.idReserva=idReserva;
            this.tarjetaCredito=tarjeta;
            this.matricula=matricula;
            this.email=email;
            this.fechaReserva=fechaRes;
            this.horaReserva=horaRes;
            this.oficinaReserva=ofiRes;
            this.fechaDestino=fechaDes;
            this.horaDestino=horaDes;
            this.oficinaDestino=ofiDes;
            this.estadoReserva=estado;
            this.costeReserva=coste;
            this.tarifaCoche=tarifaCl;
            this.tarifaCoche=tarifaCo;
            this.periodoAno=periodo;
            this.tipoCliente=tipoCliente;
            this.extrasCoche=extras;
        }
}