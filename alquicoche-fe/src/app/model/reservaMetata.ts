import { ReservaWithId } from "./reservaWithId"; 

export class ReservaMetata{

    "reservas": ReservaWithId[];

    "infoPagina": {
        "pageSize": number,
        "pageNumber": number,
        "totalPages": number
    }

}
