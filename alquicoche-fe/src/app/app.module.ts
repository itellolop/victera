import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CocheComponent } from './pages/coche/coche.component';
import { ReservaFormComponent } from './pages/reserva-form/reserva-form.component';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { RegistroComponent } from './pages/registro/registro.component';
import { ReservasComponent } from './pages/reservas/reservas.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { FiltrosComponent } from './pages/filtros/filtros.component';
import { CambioComponent } from './pages/cambio/cambio.component';


@NgModule({
  declarations: [
    AppComponent,
    CocheComponent,
    ReservaFormComponent,
    HomeComponent,
    LoginComponent,
    RegistroComponent,
    ReservasComponent,
    FiltrosComponent,
    CambioComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
