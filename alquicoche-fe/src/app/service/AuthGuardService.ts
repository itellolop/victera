import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable()
export class AuthGuardService implements CanActivate {

  constructor(private router: Router) {}
  
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    console.log(route);
    console.log(state.url);
    
    //Cuanto tengamos usuarios para loggear descomentar esto
    //asi los usuarios no loggeados no pueden entrar a filtros o coches
    if (sessionStorage.getItem("userActual")!=null) {
      return true;
    } else {
    this.router.navigate(['login']);
      return false;
    }
return true;
  }
}
