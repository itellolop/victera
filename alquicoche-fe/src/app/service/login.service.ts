import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private api: ApiService) { }

  verificacion(correo: string, contrasena: string): Observable<Boolean>{
    return this.api.get(`check/${correo}/${contrasena}`)
  }

}
