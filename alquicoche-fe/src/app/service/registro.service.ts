import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Observable } from 'rxjs/internal/Observable';
import { Usuario } from '../model/usuario';


@Injectable({
  providedIn: 'root'
})
export class RegistroService {

  constructor(public api: ApiService) { }

  agregar(usuario: Usuario): Observable <number>{
    return this.api.post(`insertUser`, usuario,);
   }



}
