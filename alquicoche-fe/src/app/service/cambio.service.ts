import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Observable } from 'rxjs/internal/Observable';
import { Cambio } from '../model/cambio';


@Injectable({
  providedIn: 'root'
})
export class CambioService {

  constructor(public api: ApiService) { }

  cambiar(cambio: Cambio): Observable <number>{    
    return this.api.post(`changePassword`,cambio);
   }



}
