import { Injectable } from '@angular/core';
import { Coche } from '../model/coche';
import { Observable } from 'rxjs';
import { ApiService } from './api.service';
import { CocheMetatata } from '../model/cocheMetatata';

@Injectable({
  providedIn: 'root'
})
export class CocheService {
  pageNum: number = 1;
  pageSize: number = 5;
  coches: CocheMetatata;

  getPageSize(): number {
    return this.pageSize;
  }
  
  getPageNum() {
    return this.pageNum;
  }

  setPageNum(num: number) {
    this.pageNum = num;
    sessionStorage.setItem('paginaActualCon', this.pageNum.toString()); //guardo en memoria la pagina en la que me encuentro para poder recargar y seguir avanzando bien
  }

  getCons() {
    return this.coches;
  }
  setCoches(coch: CocheMetatata) {
    this.coches = coch;
  }


  constructor(private api: ApiService) {}
//Añadir parametros a futuro
  getCoches(pageNumber: number, pageSize: number, oficinaRes: string, oficinaDes: string, fechaRes: string, horaRes: string, fechaDes: string, horaDes: string): Observable<CocheMetatata> {
    return this.api.get(`coches?pageSize=${pageSize}&pageNumber=${pageNumber}&oficinaReserva=${oficinaRes}&oficinaDestino=${oficinaDes}&fechaReserva=${fechaRes}&horaReserva=${horaRes}&fechaDestino=${fechaDes}&horaDestino=${horaDes}`);
  }
  getCocheByMatricula(matricula: string){
    return this.api.get(`cocheMat?matricula=${matricula}`)
  }
}