import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { Reserva } from '../model/reserva';
import { ApiService } from './api.service';
import { ReservaMetata } from '../model/reservaMetata';
import { ModifyReserva } from '../model/modifyReserva';


@Injectable({
  providedIn: 'root'
})
export class ReservaService {

  private reservasUrl: string;


  constructor(
    public api:ApiService
  ) {}
 //Acceder a reservas de usuario x

 //Guardar reserva
anadirReserva(reserva: Reserva): Observable<Reserva>{
   return this.api.post('insert', reserva);
}

getReservas(pageNumber: number, pageSize: number, email: string): Observable<ReservaMetata> {
  return this.api.get(`reservasByUser?pageSize=${pageSize}&pageNumber=${pageNumber}&email=${email}`);
}

deleteReserva(id: number): Observable<Reserva>{
  return this.api.delete(`deleteReserva/${id}`);
}

modificarReserva(id: number, modifyReserva:ModifyReserva) {
  return this.api.post(`modificar/${id}`,modifyReserva)
}

verPrecioReserva(reserva: Reserva): Observable<number>{
  return this.api.get(`precioReserva?fechaReserva=${reserva.fechaReserva}&horaReserva=${reserva.horaReserva}&oficinaReserva=${reserva.oficinaReserva}&fechaDestino=${reserva.fechaDestino}&horaDestino=${reserva.horaDestino}&oficinaDestino=${reserva.oficinaDestino}&tarifaCliente=${reserva.tarifaCliente}&tarifaCoche=${reserva.tarifaCoche}&periodoAnio=${reserva.periodoAno}&tipoCliente=${reserva.tipoCliente}&extrasCoche=${reserva.extrasCoche}`);
}

}
