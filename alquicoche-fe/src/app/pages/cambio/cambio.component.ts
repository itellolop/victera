import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Cambio } from 'src/app/model/cambio';
import { CambioService } from 'src/app/service/cambio.service';

@Component({
  selector: 'app-cambio',
  templateUrl: './cambio.component.html',
  styleUrls: ['./cambio.component.css']
})
export class CambioComponent implements OnInit {

  email: string;
  password: string;
  password2: string;
  cambio: Cambio;

  constructor(public cambioService: CambioService, public router: Router) { }

  ngOnInit(): void {
    this.cambio = new Cambio();
  }

  submit(){
    var errContrasena = document.getElementById('error-password');

    var exito = document.getElementById('exito-msg')

    if (this.password != this.password2){
      errContrasena.style.display = 'block';
    }

    this.cambio.email = this.email;
    this.cambio.password = this.password;
    this.cambioService.cambiar(this.cambio).subscribe();
    this.router.navigate(['/home']);
  }

}
