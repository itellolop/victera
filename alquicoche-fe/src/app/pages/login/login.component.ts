import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/service/login.service';

export class Datos{
  correo: string;
  contrasena: string;
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent{

  datos: Datos = new Datos();
  
  constructor(public loginService: LoginService, public router: Router) { }

  login(){
    var regexDNI = new RegExp('^[^@]+@[^@]+\.[a-zA-Z]{2,}$');
   //Verificamos que el DNI tenga un formato valido
   var errDNI = document.getElementById('error-dni');
   var errUsrContr = document.getElementById('error');
   if (!regexDNI.test(this.datos.correo)) {
     errDNI.style.display = 'block';
     errUsrContr.style.display = 'none';
   } else {
     errDNI.style.display = 'none';
     //Si lo tiene, comprobamos que el usuario-contrasena es correcto
     this.loginService.verificacion(this.datos.correo, this.datos.contrasena)
     .subscribe((bool)=>{
       if(bool){
         sessionStorage.setItem("userActual", this.datos.correo);
         this.router.navigate(['/home']);
       }else{
         errUsrContr.style.display = 'block';
       }
     });
   }

  }


}
