import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { Reserva } from 'src/app/model/reserva';
import { ReservaService } from 'src/app/service/reserva.service';

export class Datos{
  tarifa: string;
  tarjeta: string;
  wifi: string;
  gps: string;
  cadenas: string;
  silla:string;
}

export class Pepa{
  precio: number;
}

@Component({
  selector: 'app-reserva-form',
  templateUrl: './reserva-form.component.html',
  styleUrls: ['./reserva-form.component.css']
})
export class ReservaFormComponent{

  datos: Datos = new Datos();
  reserva: Reserva;
  extras: number=0;
  precio: number;

  constructor(
    private route: ActivatedRoute, 
      private router: Router, 
        private reservaService: ReservaService) {}

  submit(){

    if (this.datos.wifi) this.extras+=1;
    if (this.datos.gps) this.extras+=2;
    if (this.datos.cadenas) this.extras+=4;
    if (this.datos.silla) this.extras+=8;
    
    console.log(this.datos.tarifa);
    console.log(this.datos.tarjeta);
    console.log(this.extras);
    
    this.reserva=new Reserva(this.datos.tarjeta,sessionStorage.getItem("coche"),sessionStorage.getItem("userActual"),sessionStorage.getItem("fechaRes"),
    sessionStorage.getItem("horaRes"),sessionStorage.getItem("ciudadRes"),sessionStorage.getItem("fechaDes"),
    sessionStorage.getItem("horaDes"),sessionStorage.getItem("ciudadDes"),0,0,parseInt(this.datos.tarifa),0,-1,0,this.extras);

    this.reservaService.anadirReserva(this.reserva).subscribe();

    this.extras=0;

    this.router.navigate(['/reservas']);

    window.setTimeout(function(){location.reload()},125);
  }

  calc(){

    if (this.datos.wifi) this.extras+=1;
    if (this.datos.gps) this.extras+=2;
    if (this.datos.cadenas) this.extras+=4;
    if (this.datos.silla) this.extras+=8;
    
    console.log(this.datos.tarifa);
    console.log(this.datos.tarjeta);
    console.log(this.extras);
    
    this.reserva=new Reserva(this.datos.tarjeta,sessionStorage.getItem("coche"),sessionStorage.getItem("userActual"),sessionStorage.getItem("fechaRes"),
    sessionStorage.getItem("horaRes"),sessionStorage.getItem("ciudadRes"),sessionStorage.getItem("fechaDes"),
    sessionStorage.getItem("horaDes"),sessionStorage.getItem("ciudadDes"),0,0,parseInt(this.datos.tarifa),0,-1,0,this.extras);

    this.reservaService.verPrecioReserva(this.reserva).subscribe((numb)=>{
      this.precio = numb;
    });

    this.extras=0;

  }

}
