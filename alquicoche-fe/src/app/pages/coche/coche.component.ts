import { ThisReceiver } from '@angular/compiler';
import { Component, Injectable, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Coche } from 'src/app/model/coche';
import { CocheMetatata } from 'src/app/model/cocheMetatata';
import { CocheService } from 'src/app/service/coche.service';

@Component({
  templateUrl: './coche.component.html',
  styleUrls: ['./coche.component.css']
})

@Injectable({
  providedIn: 'root'
})
export class CocheComponent implements OnInit {

//los cogemos del sessionStorage que nos trae el filtros

coches: CocheMetatata;
matricula:string;
arrayCoches: Coche[];
pageNumber: number;
pageSize: number;
oficinaRes: string;
fechaRes: string;
horaRes: string; 
fechaDes: string;
horaDes: string;
oficinaDes: string;
mostrarOfi: string;

  constructor(private cocheService: CocheService, public router: Router) { }

  ngOnInit(): void {
    this.pageNumber=1;
    this.pageSize=39;
    this.oficinaRes=sessionStorage.getItem("ciudadRes");
    this.oficinaDes=sessionStorage.getItem("ciudadDes");
    this.fechaRes=sessionStorage.getItem("fechaRes");
    this.horaRes=sessionStorage.getItem("horaRes");
    this.fechaDes=sessionStorage.getItem("fechaDes");
    this.horaDes=sessionStorage.getItem("horaDes");

    if(this.oficinaRes=="1"){
      this.mostrarOfi="Madrid";
    }else if(this.oficinaRes=="2"){
      this.mostrarOfi="Valencia";
    }else{
      this.mostrarOfi="Toledo";
    }
    

    //en algún momento tendremos que meter los parametros con variables
    this.cocheService.getCoches(this.pageNumber, this.pageSize, this.oficinaRes, this.oficinaDes, this.fechaRes, this.horaRes, this.fechaDes, this.horaDes).subscribe((data: CocheMetatata) => {
      console.log(data);
      this.coches = data;});
  }

  elegir(matricula){
  sessionStorage.setItem("coche",matricula);
  this.router.navigate(['/reservas/formulario']);
  }
}
