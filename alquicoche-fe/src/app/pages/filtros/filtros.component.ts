import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

export class Datos{
  ciudadRes: string;
  ciudadDes: string;
  fechaRes: string;
  horaRes: string;
  fechaDes: string;
  horaDes: string;
}

export class Aux{
  hRes: string;
  mRes: string;
  yearRes: string;
  monthRes: string;
  dayRes: string;
  hDes: string;
  mDes: string;
  yearDes: string;
  monthDes: string;
  dayDes: string
}



@Component({
  selector: 'app-filtros',
  templateUrl: './filtros.component.html',
  styleUrls: ['./filtros.component.css']
})
export class FiltrosComponent implements OnInit {
  datos: Datos = new Datos();
  aux: Aux = new Aux();

  constructor(public router: Router) { }

  ngOnInit(): void {
    this.aux.monthRes="";
    this.aux.monthDes="";
  }

//dd-mm-yyyy
  filtros(){
    var regexFecha = new RegExp('((?:19|20)\\d\\d)-(0?[1-9]|1[012])-([12][0-9]|3[01]|0?[1-9])');
    var regexHour = new RegExp('^(2[0-3]|[01]?[0-9]):([0-5]?[0-9]):([0-5]?[0-9])$');
   //Verificamos que la fecha tenga un formato valido
   var errFecha = document.getElementById('error-fecha');
   var errHora = document.getElementById('error-hora');

   console.log(this.aux.hRes+":"+this.aux.mRes+":00");

  //Tratamos la hora de reserva  
  this.datos.horaRes= this.aux.hRes+":"+this.aux.mRes+":00";
  //Tratamos la hora de devolucion 
  this.datos.horaDes= this.aux.hDes+":"+this.aux.mDes+":00";
  //Tratamos la fecha reserva
  this.datos.fechaRes= this.aux.yearRes+"-"+this.aux.monthRes+"-"+this.aux.dayRes;
   //Tratamos la fecha de devolucion 
   this.datos.fechaDes= this.aux.yearDes+"-"+this.aux.monthDes+"-"+this.aux.dayDes;

   //Fecha vacia
   if (this.datos.fechaRes==null || this.datos.fechaDes==null) {
     errFecha.style.display = 'block';
     errHora.style.display = 'none';
   } else {
     errFecha.style.display = 'none';
     //Si lo tiene, comprobamos que la hora tenga formato correcto correcto
     if (!regexHour.test(this.datos.horaRes) || !regexHour.test(this.datos.horaDes)) {
      errHora.style.display = 'block';  
     } else {
     errHora.style.display = 'none';


      
         sessionStorage.setItem("ciudadRes", this.datos.ciudadRes);
         sessionStorage.setItem("ciudadDes", this.datos.ciudadDes);
         sessionStorage.setItem("fechaRes", this.datos.fechaRes);
         sessionStorage.setItem("horaRes", this.datos.horaRes);
         sessionStorage.setItem("fechaDes", this.datos.fechaDes);
         sessionStorage.setItem("horaDes", this.datos.horaDes);
         this.router.navigate(['/coches']);
       }
     }
   }

  }


