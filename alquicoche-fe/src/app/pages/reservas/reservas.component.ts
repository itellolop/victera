import { Component, Injectable, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {ReservaMetata} from 'src/app/model/reservaMetata'
import { ReservaWithId } from 'src/app/model/reservaWithId';
import { ReservaService } from 'src/app/service/reserva.service';
import { CocheService } from 'src/app/service/coche.service';
import { Coche } from 'src/app/model/coche';
import { ModifyReserva } from 'src/app/model/modifyReserva';



export class Datos{
  tarifa: string;
  tarjeta: string;
  wifi: string;
  gps: string;
  cadenas: string;
  silla:string;
}

@Component({
  selector: 'app-reservas',
  templateUrl: './reservas.component.html',
  styleUrls: ['./reservas.component.css']
})

@Injectable({
  providedIn: 'root'
})
export class ReservasComponent implements OnInit {
  
  reservas: ReservaMetata;
  pageNumber: number;
  pageSize: number;
  email: string;
  reserva: ReservaWithId;
  marca: string;
  modelo: string;
  gama: string;
  matricula: string;
  datos: Datos;
  extras: number;
  modifyReserva : ModifyReserva;

  constructor(private reservaService: ReservaService, private cocheService: CocheService,  public router: Router) { }

  ngOnInit(): void {
    this.reserva = new ReservaWithId("","","","","","","","","","","","","","","","","");
    this.datos = new Datos();
    this.modifyReserva = new ModifyReserva();
    this.extras = 0;
    this.pageNumber=1;
    this.pageSize=10;
    this.email = sessionStorage.getItem("userActual");
    this.reservaService.getReservas(this.pageNumber, this.pageSize,this.email).subscribe((data: ReservaMetata) => {
      console.log(data);
      this.reservas = data;});


  }

  elegir(reserva){
    this.reserva = reserva;
    this.cocheService.getCocheByMatricula(reserva.matricula).subscribe((data: Coche) => {
      console.log(data);
      this.modelo = data.modelo;
      this.gama = data.gama;
      this.marca = data.marca;});
    }
  
  borrarReserva(id){
    this.reservaService.deleteReserva(id).subscribe();
    window.location.reload();
  }

  submit(){

    this.extras=0;
    if (this.datos.wifi) this.extras+=1;
    if (this.datos.gps) this.extras+=2;
    if (this.datos.cadenas) this.extras+=4;
    if (this.datos.silla) this.extras+=8;
    
    console.log(this.datos.tarifa);
    console.log(this.datos.tarjeta);
    console.log(this.extras);

    this.modifyReserva.extrasCoche = this.extras;
    this.modifyReserva.tarifaCliente = parseInt(this.datos.tarifa);
    this.modifyReserva.tarjetaCredito = this.datos.tarjeta;


    this.reservaService.modificarReserva(this.reserva.idReserva,this.modifyReserva).subscribe();
    window.setTimeout(function(){location.reload()},125);
    
  }


}
