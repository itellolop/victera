import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  title = 'alquicoche-fe';
  hoy:string;
  proxSemana:string;

  constructor(private route: Router) {
    this.title = 'Alquicoche - Alquiler de coches online';
  }

  ngOnInit(): void {
   
  }

  reservas(){
    this.route.navigate(['reservas']);
  }
  filtros(){
    this.route.navigate(['filtros']);
  }
  login(){
    this.route.navigate(['login']);
  }
  registro(){
    this.route.navigate(['registro']);
  }
  verCoches(){
    sessionStorage.setItem("ciudadRes","1");
    sessionStorage.setItem("ciudadDes","1");
    sessionStorage.setItem("fechaRes","2022-05-20");
    sessionStorage.setItem("horaRes","10:00:00");
    sessionStorage.setItem("fechaDes","2022-05-27");
    sessionStorage.setItem("horaDes","10:00:00");
    this.route.navigate(['/coches']);
  }

}
