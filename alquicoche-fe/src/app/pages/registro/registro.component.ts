import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { RegistroService } from 'src/app/service/registro.service';
import { Usuario } from '../../model/usuario';


@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent {

  usuario: Usuario = new Usuario();
  password2: string;

  constructor(public registroService: RegistroService, public router: Router) { }

  registro(){
    var regexDNI = new RegExp ('^[0-9]{8}[TRWAGMYFPDXBNJZSQVHLCKET]{1}$');
    var regexCorreo = new RegExp('^[^@]+@[^@]+\.[a-zA-Z]{2,}$');
    var regexNames = new RegExp("^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z])$");
    //Verificamos que el DNI tenga un formato valido
    var errNombre = document.getElementById('error-nombre');
    var errApellido = document.getElementById('error-apellido');
    var errDNI = document.getElementById('error-dni');
    var errCorreo = document.getElementById('error-mail');
    var errContrasena = document.getElementById('error-password');
    var errDNIExiste = document.getElementById('error-dni-existe');
    var errCorreoExiste = document.getElementById('error-email-existe');
    errNombre.style.display = 'none';
    errApellido.style.display = 'none';
    errDNI.style.display = 'none';
    errCorreo.style.display = 'none';
    errContrasena.style.display = 'none';
    errCorreoExiste.style.display = 'none';
    errDNIExiste.style.display = 'none';
 
    if (!regexNames.test(this.usuario.nombre)) {
      errNombre.style.display = 'block';
    }
    else if (!regexNames.test(this.usuario.apellido)) {
      errApellido.style.display = 'block';
    }
    else if (!regexDNI.test(this.usuario.dni)) {
      errDNI.style.display = 'block';
    } 
    else if (!regexCorreo.test(this.usuario.email)) {
      errCorreo.style.display = 'block';
    }
    else if (this.usuario.password != this.password2){
      errContrasena.style.display = 'block';
    }

    this.registroService.agregar(this.usuario)
       .subscribe((numbi)=>{
      if(numbi == 1){
        errCorreoExiste.style.display = 'block';
        
      }else if (numbi == 2){
        errDNIExiste.style.display = 'block';
        
      }else if (numbi == 0){
        sessionStorage.setItem("userActual", this.usuario.email);
        this.router.navigate(['/home']);

      }
    });
  }

}
