import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CocheComponent } from './pages/coche/coche.component';
import { ReservasComponent } from './pages/reservas/reservas.component';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { ReservaFormComponent } from './pages/reserva-form/reserva-form.component';
import { FiltrosComponent } from './pages/filtros/filtros.component';
import { AuthGuardService } from './service/AuthGuardService';
import { RegistroComponent } from './pages/registro/registro.component';
import { CambioComponent } from './pages/cambio/cambio.component';

const routes: Routes = [
{ path: 'home', component: HomeComponent },
{ path: 'coches', component: CocheComponent},
{ path: 'reservas', component: ReservasComponent,canActivate:[AuthGuardService]},
{ path: 'login', component: LoginComponent },
{ path: 'registro', component: RegistroComponent},
{ path: 'cambio', component: CambioComponent},
{ path: 'filtros', component: FiltrosComponent},
{ path: 'reservas/formulario', component: ReservaFormComponent,canActivate:[AuthGuardService]},
{ path: '', redirectTo: 'home', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers:[AuthGuardService],
})
export class AppRoutingModule { }
