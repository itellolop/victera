package com.victera.alquicoche.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class CronService {

    @Autowired
    Mail mail;
    //Se comprueba que el sistema de correos funciona correctamente cada hora
    @Scheduled(cron = "0 * * * *")
    public void metricSending(){
        try {
            mail.sendMail(
                    "alquicochevictera@gmail.com",
                    "El correo funciona",
                    "<div><h1>Alquicoche</h1><p>El sistema de correos funciona correctamente. </p></div>"
            );
        } catch (Exception e) {
            e.printStackTrace();
        }
     

    }
}
