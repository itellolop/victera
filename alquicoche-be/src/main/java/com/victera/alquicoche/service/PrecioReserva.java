package com.victera.alquicoche.service;

import com.victera.alquicoche.model.Reserva;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.lang.Math;

@Service
public class PrecioReserva {

    @Autowired
    PeriodoAnio periodoAnioService;

    public double calcular_precio_reserva(Reserva res){
        double coste = -1;
        double precio_limitado = 42.47;
        double precio_ilimitado = 60.67;

        int tarifaCliente = res.getTarifaCliente();
        int periodoAnio = res.getPeriodoAnio();
        int tarifaCoche = res.getTarifaCoche();
        int tipoCliente = res.getTipoCliente();

        String extras = Integer.toBinaryString(res.getExtrasCoche());

	    while(extras.length()<4){
	        extras = '0' + extras;
	    }

        long dif = (res.getFechaDestino().getTime()-res.getFechaReserva().getTime())/86400000;
        int duracion = (int) Math.abs(dif);
        
        switch(tarifaCliente){
            case 0:
                coste = precio_limitado * duracion;
                break;
            case 1:
                coste = precio_ilimitado * duracion;
                break;
            case 2:
                coste = precio_ilimitado * 6;
                break;
            case 3:
                coste = (precio_ilimitado * 3)-20;
                break;
            case 4:
                coste = precio_ilimitado * 25;
                break;
        }
        
        if (periodoAnio == -1){
            periodoAnio = periodoAnioService.getPeriodoAnio(res.getFechaReserva());
        }

        switch(periodoAnio){
            case 0:
                break;
            case 1:
                coste *= 1.1;
                break;
            case 2:
                coste *= 0.9;
                break;
        }

        switch(tarifaCoche){
            case 0:
                coste *= 0.5;
                break;
            case 1:
                break;
            case 2:
                coste *= 1.5;
                break;
        }

        if(tipoCliente==1){
            coste *= 0.8;
        }
        
        if(extras.charAt(0)=='1') coste += 69;
        if(extras.charAt(1)=='1') coste += 63;
        if(extras.charAt(2)=='1') coste += 63;
        if(extras.charAt(3)=='1') coste += 72;
        
        return coste;
    }

}