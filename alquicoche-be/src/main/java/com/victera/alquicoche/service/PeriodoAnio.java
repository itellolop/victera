package com.victera.alquicoche.service;

import org.springframework.stereotype.Service;
import java.text.SimpleDateFormat;

@Service
public class PeriodoAnio {
    /*
    Function that given a date in java.sql.Date format, returns:
    - 0 if the month is between january and may
    - 1 if the month is between june and september
    - 2 if the month is between october and december
    */
    public int getPeriodoAnio(java.sql.Date fechaReserva) {
        SimpleDateFormat sdf = new SimpleDateFormat("MM");
        int mes = Integer.parseInt(sdf.format(fechaReserva));
        if (mes >= 1 && mes <= 5) {
            return 0;
        } else if (mes >= 6 && mes <= 9) {
            return 1;
        } else {
            return 2;
        }
    }
}