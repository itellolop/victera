package com.victera.alquicoche.model;

public class Coche {
    private String matricula;
    private String modelo;
    private String marca;
    private String gama;
    private String oficina;
    private String cambio;
    private int puertas;
    private Boolean techoSolar;
    private Boolean disponible;

    public Coche() {
    }

    public Coche(String matricula, String modelo, String marca, String gama, String oficina, String cambio, int puertas, Boolean techoSolar, Boolean disponible) {
        this.matricula = matricula;        
        this.modelo = modelo;
        this.marca = marca;
        this.gama = gama;
        this.oficina = oficina;
        this.cambio = cambio;
        this.puertas = puertas;
        this.techoSolar = techoSolar;
        this.disponible = disponible;
    }

    public String getMatricula() {
        return this.matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }
    
    public String getModelo() {
        return this.modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getMarca() {
        return this.marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getGama() {
        return this.gama;
    }

    public void setGama(String gama) {
        this.gama = gama;
    }

    public String getOficina() {
        return this.oficina;
    }

    public void setOficina(String oficina) {
        this.oficina = oficina;
    }

    public String getCambio() {
        return this.cambio;
    }

    public void setCambio(String cambio) {
        this.cambio = cambio;
    }

    public int getPuertas() {
        return this.puertas;
    }

    public void setPuertas(int puertas) {
        this.puertas = puertas;
    }

    public Boolean getTechoSolar() {
        return this.techoSolar;
    }

    public void setTechoSolar(Boolean techoSolar) {
        this.techoSolar = techoSolar;
    }

    public Boolean getDisponible() {
        return this.disponible;
    }

    public void setDisponible(Boolean disponible) {
        this.disponible = disponible;
    }
}