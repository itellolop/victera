package com.victera.alquicoche.model.response;

import com.victera.alquicoche.model.User;

import java.util.List;

public class ListUserResponse {

    private List<User> users;
    private Metadata metadata;

    public ListUserResponse() {
    }

    public ListUserResponse(List<User> users, Metadata metadata) {
        this.users = users;
        this.metadata = metadata;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public Metadata getMetadata() {
        return metadata;
    }

    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }
}
