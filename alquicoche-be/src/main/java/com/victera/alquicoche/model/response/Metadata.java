package com.victera.alquicoche.model.response;


import javax.validation.constraints.NotNull;

public class Metadata {

    @NotNull
    private Integer pageNumber;

    @NotNull
    private Integer pageSize;

    private Integer totalPages;

    public Metadata() {
    }

    public Metadata(@NotNull Integer pageNumber, @NotNull Integer pageSize, Integer totalPages) {
        this.pageNumber = pageNumber;
        this.pageSize = pageSize;
        this.totalPages = totalPages;
    }

    public Integer getPageNumber() {
        return pageNumber;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public Integer getTotalPages() {
        return totalPages;
    }

    public void setPageNumber(Integer pageNumber) {
        this.pageNumber = pageNumber;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }
}
