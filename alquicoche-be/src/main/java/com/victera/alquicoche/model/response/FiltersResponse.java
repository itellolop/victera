package com.victera.alquicoche.model.response;

import javax.validation.constraints.NotNull;
import java.sql.Date;
import java.sql.Time;

public class FiltersResponse {

    @NotNull
    private String oficinaReserva;

    @NotNull
    private Date fechaReserva;

    @NotNull
    private Time horaReserva;

    @NotNull
    private String oficinaDestino;

    @NotNull
    private Date fechaDestino;

    @NotNull
    private Time horaDestino;

    private String gama;
    private String cambio;
    private Integer puertas;
    private Boolean techoSolar;

    public FiltersResponse() {
    }

    public FiltersResponse(String oficinaReserva, Date fechaReserva, Time horaReserva, String oficinaDestino, Date fechaDestino, Time horaDestino, String gama, String cambio, Integer puertas, Boolean techoSolar) {
        this.oficinaReserva = oficinaReserva;
        this.fechaReserva = fechaReserva;
        this.horaReserva = horaReserva;
        this.oficinaDestino = oficinaDestino;
        this.fechaDestino = fechaDestino;
        this.horaDestino = horaDestino;
        this.gama = gama;
        this.cambio = cambio;
        this.puertas = puertas;
        this.techoSolar = techoSolar;
    }

    public String getOficinaReserva() {
        return oficinaReserva;
    }

    public void setOficinaReserva(String oficinaReserva) {
        this.oficinaReserva = oficinaReserva;
    }

    public Date getFechaReserva() {
        return fechaReserva;
    }

    public void setFechaReserva(Date fechaReserva) {
        this.fechaReserva = fechaReserva;
    }

    public Time getHoraReserva() {
        return horaReserva;
    }

    public void setHoraReserva(Time horaReserva) {
        this.horaReserva = horaReserva;
    }

    public String getOficinaDestino() {
        return oficinaDestino;
    }

    public void setOficinaDestino(String oficinaDestino) {
        this.oficinaDestino = oficinaDestino;
    }

    public Date getFechaDestino() {
        return fechaDestino;
    }

    public void setFechaDestino(Date fechaDestino) {
        this.fechaDestino = fechaDestino;
    }

    public Time getHoraDestino() {
        return horaDestino;
    }

    public void setHoraDestino(Time horaDestino) {
        this.horaDestino = horaDestino;
    }

    public String getGama() {
        return gama;
    }

    public void setGama(String gama) {
        this.gama = gama;
    }

    public String getCambio() {
        return cambio;
    }

    public void setCambio(String cambio) {
        this.cambio = cambio;
    }

    public Integer getPuertas() {
        return puertas;
    }

    public void setPuertas(Integer puertas) {
        this.puertas = puertas;
    }

    public Boolean getTechoSolar() {
        return techoSolar;
    }

    public void setTechoSolar(Boolean techoSolar) {
        this.techoSolar = techoSolar;
    }
}
