package com.victera.alquicoche.model.response;

import java.util.List;
import com.victera.alquicoche.model.Reserva;

public class ListReservaResponse {
    private List<Reserva> reservas;
    private Metadata metadata;

    public ListReservaResponse() {
    }

    public ListReservaResponse(List<Reserva> reservas, Metadata metadata) {
        this.reservas = reservas;
        this.metadata = metadata;
    }

    public List<Reserva> getReservas() {
        return reservas;
    }

    public void setReservas(List<Reserva> reservas) {
        this.reservas = reservas;
    }

    public Metadata getMetadata() {
        return metadata;
    }

    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }
}