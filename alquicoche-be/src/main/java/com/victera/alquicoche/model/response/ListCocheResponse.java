package com.victera.alquicoche.model.response;

import java.util.List;
import com.victera.alquicoche.model.Coche;

public class ListCocheResponse {

    private List<Coche> coches;
    private Metadata metadata;

    public ListCocheResponse() {
    }

    public ListCocheResponse(List<Coche> coches, Metadata metadata) {
        this.coches = coches;
        this.metadata = metadata;
    }

    public List<Coche> getCoches() {
        return coches;
    }

    public void setCoches(List<Coche> coches) {
        this.coches = coches;
    }

    public Metadata getMetadata() {
        return metadata;
    }

    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }

}
