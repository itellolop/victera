package com.victera.alquicoche.model.response;

public class ModifReservaResponse {
    private String tarjetaCredito;
    private String matricula;
    private int tarifaCliente;
    private int extrasCoche;

    public ModifReservaResponse(String tarjetaCredito, String matricula, int tarifaCliente, int extrasCoche){
        this.tarjetaCredito=tarjetaCredito;
        this.matricula=matricula;
        this.tarifaCliente=tarifaCliente;
        this.extrasCoche=extrasCoche;
    }

    public String getTarjetaCredito(){
        return this.tarjetaCredito;
    }

    public void setTarjetaCredito(String tarjetaCredito){
        this.tarjetaCredito=tarjetaCredito;
    }

    public String getMatricula(){
        return this.matricula;
    }

    public void setMatricula(String matricula){
        this.matricula=matricula;
    }

    public int getTarifaCliente(){
        return this.tarifaCliente;
    }

    public void setTarifaCliente(int tarifaCliente){
        this.tarifaCliente=tarifaCliente;
    }

    public int getExtrasCoche(){
        return this.extrasCoche;
    }

    public void setExtrasCoche(int extrasCoche){
        this.extrasCoche=extrasCoche;
    }
}
