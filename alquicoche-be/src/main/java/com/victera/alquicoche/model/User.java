package com.victera.alquicoche.model;

public class User {
    private String dni;
    private String email;
    private String password;
    private String nombre;
    private String apellido;
    private int esAdmin;

    public User() {
    }

    public User(String dni, String email, String password, String nombre, String apellido) {
        this.dni = dni;
        this.email = email;
        this.password = password;
        this.nombre = nombre;
        this.apellido = apellido;
    }

    public String getDni() {
        return dni;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public int getEsAdmin(){
        return esAdmin;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public void setEsAdmin(int esAdmin){
        this.esAdmin = esAdmin;
    }

}