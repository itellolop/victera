package com.victera.alquicoche;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AlquicocheApplication {

	public static void main(String[] args) {
		SpringApplication.run(AlquicocheApplication.class, args);
	}

}
