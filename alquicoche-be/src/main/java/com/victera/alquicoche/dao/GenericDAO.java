package com.victera.alquicoche.dao;

import java.sql.Connection;
import java.util.List;

import com.victera.alquicoche.model.response.Metadata;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.victera.alquicoche.dao.connector.Connector;


@Repository
public class GenericDAO {

    @Autowired
    protected Connector connector;

    @Autowired
    protected QueryRunner queryRunner;

    protected String paginationQuery(Metadata metadata){
        return String.format("LIMIT %d OFFSET %d ",
                metadata.getPageSize(),
                metadata.getPageSize()*(metadata.getPageNumber()-1));
    }


    public <T> List<T> getPagesFromQuery(Metadata metadata, String query, Class<T> type) throws Exception{
        List<T> result;
        String finalQuery = String.format("%s %s",
                query,
                paginationQuery(metadata)
        );
        try(Connection conn = connector.getConnection()){
            result = queryRunner.query(conn,finalQuery, new BeanListHandler<>(type));

        }
        return result;
    }


    public Integer numOfRows(String table, String condition) throws Exception {
        Long result= (long) 0;
        String query = String.format("SELECT COUNT(*) FROM %s %s ", table,condition);
        try(Connection conn = connector.getConnection()){
            result = queryRunner.query(conn, query, new ScalarHandler<Long>());
        }

        return result.intValue();


    }

    public <T> List<T> getPagesfromAll(Metadata metadata, String table, Class<T> type) throws Exception{
        String query = String.format("SELECT * FROM %s ",table );
        return getPagesFromQuery(metadata,query,type);
    }



}
