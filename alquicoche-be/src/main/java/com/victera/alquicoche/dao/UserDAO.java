package com.victera.alquicoche.dao;

import com.victera.alquicoche.dao.connector.Connector;
import com.victera.alquicoche.model.User;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.Connection;

@Repository
public class UserDAO extends GenericDAO {

    @Autowired
    protected Connector connector;

    @Autowired
    protected QueryRunner queryRunner;

    public void insertUser(User user) throws Exception{
        String query = "INSERT INTO `bzkcsila4xkbpgj6bnhh`.`users` (email, password, nombre, apellido, dni) VALUES (?,?,?,?,?)";
        try (Connection conn = connector.getConnection()) {
            queryRunner.insert(conn, query,new ScalarHandler<>(),user.getEmail(),user.getPassword(),user.getNombre(),user.getApellido(),user.getDni());
        }
    }

    public User getUserWithEmail(String email) throws Exception {
        String query =
                "SELECT * FROM users where email = \"" + email + "\";";
        try (Connection conn = connector.getConnection()) {
            return queryRunner.query(conn, query, new BeanHandler<>(User.class));
        }

    }

    public User getUserByDNI(String dni) throws Exception{
        String query =
                "SELECT * FROM users where dni = \"" + dni + "\";";
        try (Connection conn = connector.getConnection()) {
            return queryRunner.query(conn, query, new BeanHandler<>(User.class));
        }
    }

    public void updatePasswordUser(String email, String newPassword) throws Exception{
        String query = "UPDATE `bzkcsila4xkbpgj6bnhh`.`users`"+
                "SET `password` = ? WHERE (`email` = ?)";
        try (Connection conn = connector.getConnection()) {
            queryRunner.update(conn, query, newPassword, email);
        }
    }

    public void deleteUser(User user) throws Exception{
        String query = "DELETE FROM `bzkcsila4xkbpgj6bnhh`.`users`"+
                "WHERE (`email` = ?)";
        try (Connection conn = connector.getConnection()) {
            queryRunner.update(conn, query,user.getEmail());
        }
    }

}