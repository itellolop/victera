package com.victera.alquicoche.dao;

import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.apache.commons.dbutils.QueryRunner;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import com.victera.alquicoche.model.Reserva;
import com.victera.alquicoche.model.response.Metadata;
import com.victera.alquicoche.model.response.ReservaResponse;
import com.victera.alquicoche.dao.connector.Connector;
import org.springframework.stereotype.Repository;

@Repository
public class ReservaDAO extends GenericDAO {

    @Autowired
    protected Connector connector;

    @Autowired
    protected QueryRunner queryRunner;

    public List<Reserva> reservaList() throws SQLException {

        String query = "SELECT * FROM `bzkcsila4xkbpgj6bnhh`.`reserva`";
        List<Reserva> res = new LinkedList<>();

        try (Connection conn = connector.getConnection()) {
            res = queryRunner.query(conn, query, new BeanListHandler<>(Reserva.class));
        }
        
        return res;
    }

    public void insertReservaWithId(Reserva reserva) throws SQLException {

        String query = "INSERT INTO `bzkcsila4xkbpgj6bnhh`.`reserva` (`idReserva`,`tarjetaCredito`, `matricula`,`email`, `fechaReserva`, `horaReserva`, `oficinaReserva`, `fechaDestino`, `horaDestino`, `oficinaDestino`, `estadoReserva`, `costeReserva`, `tarifaCliente`, `tarifaCoche`, `periodoAnio`, `tipoCliente`, `extrasCoche`) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                
        try (Connection conn = connector.getConnection()) {
            queryRunner.insert(conn, query, new ScalarHandler<>(), reserva.getIdReserva(),
                    reserva.getTarjetaCredito(),reserva.getMatricula(),reserva.getEmail(),reserva.getFechaReserva(),reserva.getHoraReserva(),reserva.getOficinaReserva(),
                    reserva.getFechaDestino(),reserva.getHoraDestino(),reserva.getOficinaDestino(),reserva.getEstadoReserva(),reserva.getCosteReserva(),
                    reserva.getTarifaCliente(), reserva.getTarifaCoche(),reserva.getPeriodoAnio(),reserva.getTipoCliente(),reserva.getExtrasCoche());
        }

    }

    public void insertReserva(Reserva reserva) throws SQLException {

        String query = "INSERT INTO `bzkcsila4xkbpgj6bnhh`.`reserva` (`tarjetaCredito`, `matricula`,`email`, `fechaReserva`, `horaReserva`, `oficinaReserva`, `fechaDestino`, `horaDestino`, `oficinaDestino`, `estadoReserva`, `costeReserva`, `tarifaCliente`, `tarifaCoche`, `periodoAnio`, `tipoCliente`, `extrasCoche`) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

        try (Connection conn = connector.getConnection()) {
            queryRunner.insert(conn, query, new ScalarHandler<>(),
                    reserva.getTarjetaCredito(),reserva.getMatricula(),reserva.getEmail(),reserva.getFechaReserva(),reserva.getHoraReserva(),reserva.getOficinaReserva(),
                    reserva.getFechaDestino(),reserva.getHoraDestino(),reserva.getOficinaDestino(),reserva.getEstadoReserva(),reserva.getCosteReserva(),
                    reserva.getTarifaCliente(), reserva.getTarifaCoche(),reserva.getPeriodoAnio(),reserva.getTipoCliente(),reserva.getExtrasCoche());
        }

    }

    public void actualizarReserva(Reserva reserva) throws SQLException {

        String query = "UPDATE `bzkcsila4xkbpgj6bnhh`.`reserva` SET tarjetaCredito = ?, matricula = ?, fechaReserva = ?, horaReserva = ?, oficinaReserva = ?, fechaDestino = ?, horaDestino = ?, oficinaDestino = ?, estadoReserva = ?, costeReserva = ?, tarifaCliente = ?, tarifaCoche = ?, periodoAnio = ?, tipoCliente = ?, extrasCoche = ? WHERE idReserva = ?";
                
        try (Connection conn = connector.getConnection()) {
            queryRunner.insert(conn, query, new ScalarHandler<>(), reserva.getTarjetaCredito(), reserva.getMatricula(), reserva.getFechaReserva(), reserva.getHoraReserva(), reserva.getOficinaReserva(), reserva.getFechaDestino(), reserva.getHoraDestino(), reserva.getOficinaDestino(), reserva.getEstadoReserva(), reserva.getCosteReserva(), reserva.getTarifaCliente(), reserva.getTarifaCoche(), reserva.getPeriodoAnio(), reserva.getTipoCliente(), reserva.getExtrasCoche(), reserva.getIdReserva());
        }
        
    }

    public Reserva getReservaFromIdReserva(int idReserva) throws SQLException {

        String query = String.format("SELECT * FROM `bzkcsila4xkbpgj6bnhh`.`reserva` WHERE idReserva = %d", idReserva);
        
        try (Connection conn = connector.getConnection()) {
            return queryRunner.query(conn, query, new BeanHandler<>(Reserva.class));
        }
        
    }

    public void deleteReserva(int idReserva) throws Exception{

        String query = "DELETE FROM `bzkcsila4xkbpgj6bnhh`.`reserva` WHERE (`idReserva` = ?)";

        try (Connection conn = connector.getConnection()) {
            queryRunner.update(conn, query, idReserva);
        }

    }

    public List<Reserva> getReservasFromUserId(String email) throws SQLException {

        String query = "SELECT * FROM `bzkcsila4xkbpgj6bnhh`.`reserva` WHERE email = ?";
        List<Reserva> res = new LinkedList<>();

        try (Connection conn = connector.getConnection()) {
            res = queryRunner.query(conn, query, new BeanListHandler<>(Reserva.class), email);
        }
        
        return res;
    }

    public String joinIdUserReservaCoche(String mail){
        return String.format("LEFT JOIN bzkcsila4xkbpgj6bnhh.coches "
                + "ON reserva.matricula = coches.matricula "
                + "LEFT JOIN bzkcsila4xkbpgj6bnhh.users "
                + "ON users.email = reserva.email "
                + "WHERE (users.email='%s')",mail);
    }

    public List<Reserva> getReservaResponseFromUserId(Metadata metadata, String mail) throws Exception {
        String query = "SELECT * "
                + "FROM reserva "
                + joinIdUserReservaCoche(mail);

        return getPagesFromQuery(metadata, query, Reserva.class);
    }

}