package com.victera.alquicoche.dao;

import com.victera.alquicoche.model.response.FiltersResponse;
import com.victera.alquicoche.model.response.Metadata;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.apache.commons.dbutils.QueryRunner;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import com.victera.alquicoche.model.Coche;
import com.victera.alquicoche.dao.connector.Connector;
import org.springframework.stereotype.Repository;

@Repository
public class CocheDAO extends GenericDAO {

    @Autowired
    protected Connector connector;

    @Autowired
    protected QueryRunner queryRunner;

    public List<Coche> cochesList() throws SQLException {

        String query = "SELECT * FROM `bzkcsila4xkbpgj6bnhh`.`coches`";
        List<Coche> res = new LinkedList<>();

        try (Connection conn = connector.getConnection()) {
            res = queryRunner.query(conn, query, new BeanListHandler<>(Coche.class));
        }
        
        return res;
    }

    public void insertarCoche(Coche coche) throws SQLException {

        String query = "INSERT INTO `bzkcsila4xkbpgj6bnhh`.`coches` (matricula, modelo, marca, gama, oficina, cambio, puertas, techoSolar, disponible) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
                
        try (Connection conn = connector.getConnection()) {
            queryRunner.insert(conn, query, new ScalarHandler<>(),
                    coche.getMatricula(),coche.getModelo(),coche.getMarca(),coche.getGama(),coche.getOficina(),coche.getCambio(),coche.getPuertas(),coche.getTechoSolar(),coche.getDisponible());
        }

    }

    public void actualizarCoche(Coche coche) throws SQLException {

        String query = "UPDATE `bzkcsila4xkbpgj6bnhh`.`coches` SET modelo = ?, marca = ?, gama = ?, oficina = ?, cambio = ?, puertas = ?, techoSolar = ?, disponible = ? WHERE matricula = ?";
                
        try (Connection conn = connector.getConnection()) {
            queryRunner.insert(conn, query, new ScalarHandler<>(), coche.getModelo(), coche.getMarca(), coche.getGama(), coche.getOficina(), coche.getCambio(), coche.getPuertas(), coche.getTechoSolar(), coche.getDisponible(), coche.getMatricula());
        }
        
    }

    public void updateDisponibleAndOficina(String matricula, int disponible, String oficina) throws Exception{
        String query ="UPDATE  `bzkcsila4xkbpgj6bnhh`.`coches` SET disponible = ?,oficina = ? WHERE matricula = ?";

        try (Connection conn = connector.getConnection()) {
            queryRunner.update(conn, query,disponible,oficina,matricula);
        }
    }

    public Coche getCocheFromMatricula(String matricula) throws SQLException {

        String query = "SELECT * FROM `bzkcsila4xkbpgj6bnhh`.`coches` WHERE matricula = ?";
        
        try (Connection conn = connector.getConnection()) {
            return queryRunner.query(conn, query, new BeanHandler<>(Coche.class), matricula);
        }

    }

    public void deleteCoche(String matricula) throws Exception{

        String query = "DELETE FROM `bzkcsila4xkbpgj6bnhh`.`coches` WHERE (`matricula` = ?)";

        try (Connection conn = connector.getConnection()) {
            queryRunner.update(conn, query, matricula);
        }

    }

    public String joinOficinaFechaQuery(FiltersResponse filters){
        String oficinaReserva = filters.getOficinaReserva();
        String fechaReserva = filters.getFechaReserva().toString();
        return String.format(" LEFT JOIN bzkcsila4xkbpgj6bnhh.reserva " +
                        " ON coches.matricula = reserva.matricula " +
                        " WHERE ((coches.oficina='%s' AND coches.disponible='1') OR (reserva.fechaDestino < '%s' AND reserva.oficinaDestino = '%s') " +
                        "OR (reserva.fechaDestino = '%s' AND reserva.horaDestino <= '%s' AND reserva.oficinaDestino = '%s')) %s"
                ,oficinaReserva,fechaReserva,oficinaReserva,fechaReserva,filters.getHoraReserva().toString(),
                oficinaReserva, addFilters(filters));
    }

    public  List<Coche> getPagesfromJoinOficeDate(Metadata metadata, String table,FiltersResponse filters) throws Exception{
        String query = String.format("SELECT coches.matricula, coches.modelo, coches.marca, coches.gama, coches.oficina, coches.cambio, coches.puertas, coches.techoSolar, coches.disponible FROM %s %s ",table, joinOficinaFechaQuery(filters));
        return getPagesFromQuery(metadata,query,Coche.class);
    }

    public String addFilters(FiltersResponse filters){
        String res = "";
        if(filters.getGama() != null)
            res+=String.format(" AND gama = '%s'",filters.getGama());
        if(filters.getTechoSolar() != null)
            res+=String.format(" AND techoSolar = %s",filters.getTechoSolar());
        if(filters.getCambio() != null)
            res+=String.format(" AND cambio = '%s'",filters.getCambio());
        if(filters.getPuertas() != null)
            res+=String.format(" AND puertas = '%s'",filters.getPuertas());
        return res;
    }

}
