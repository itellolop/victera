package com.victera.alquicoche.controller;

import com.victera.alquicoche.dao.UserDAO;

import com.victera.alquicoche.model.response.HealthResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;





@RestController
public class HealthController {


    @GetMapping("/status")
    public ResponseEntity<HealthResponse> alive()  throws Exception{
        HealthResponse healthResponse = new HealthResponse("alive v1");
        return ResponseEntity.ok().body(healthResponse);
    }
}