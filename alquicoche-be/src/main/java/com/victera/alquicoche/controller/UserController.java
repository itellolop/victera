package com.victera.alquicoche.controller;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.victera.alquicoche.dao.UserDAO;
import com.victera.alquicoche.model.User;
import com.victera.alquicoche.model.response.ListUserResponse;
import com.victera.alquicoche.model.response.Metadata;
import com.victera.alquicoche.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class UserController {

    @Autowired
    UserDAO userDAO;

    @Autowired
    UserService userService;

    @GetMapping("/users")
    public ResponseEntity<ListUserResponse> getUsers(@Valid Metadata metadata) throws Exception{
        boolean result = metadata.getPageNumber()>0 && metadata.getPageSize()>0;

        List<User> res = null;

        if(result){

            res = userDAO.getPagesfromAll   (
                    metadata,
                    "users",
                    User.class
            );
            Integer totalRows = userDAO.numOfRows("users","");
            double div = Math.ceil((double) totalRows / metadata.getPageSize());
            metadata.setTotalPages((int) div);

        }
        ListUserResponse cocheResponse = new ListUserResponse(res,metadata);

        result = result && metadata.getPageNumber()<= metadata.getTotalPages();

        return result
                ?ResponseEntity.ok().body(cocheResponse)
                :ResponseEntity.badRequest().build();

    }

    @GetMapping("/userByEmail")
    public ResponseEntity<User> getUserByEmail(@RequestParam String email) throws Exception{

        boolean result = userService.paternMatches(email, userService.getRegexPatternForEmails());
        User res = null;
        if(result)
             res = userDAO.getUserWithEmail(email);

        return result
                ? ResponseEntity.ok().body(res)
                : ResponseEntity.badRequest().build();

    }



    @PostMapping("/changePassword")
    public void changePassword(@RequestBody ObjectNode json) throws Exception {

        String email = json.get("email").asText();
        String password = json.get("password").asText();

        boolean result = userService.paternMatches(email, userService.getRegexPatternForEmails());
        if(result)
            userDAO.updatePasswordUser(email,password);


    }

    @PostMapping("/insertUser")
    public ResponseEntity<Integer> insertUser(@RequestBody ObjectNode json) throws Exception{

        String email = json.get("email").asText();
        String password = json.get("password").asText();
        String nombre = json.get("nombre").asText();
        String apellido = json.get("apellido").asText();
        String dni = json.get("dni").asText();

        // añadir si está repetido el email o el DNI
        Integer res = 0;

        boolean emailRepeated = userDAO.getUserWithEmail(email) != null;

        boolean dniRepeated = userDAO.getUserByDNI(dni) != null;

        if(emailRepeated)
            res =  1;
        else if(dniRepeated)
            res = 2;
        else{
            User user = new User(dni,email,password,nombre,apellido);
            userDAO.insertUser(user);
            res = 0;
        }

        return ResponseEntity.ok().body(res);

    }

    @GetMapping("/check/{email}/{password}")
    public ResponseEntity<Boolean> getCheckCredentials(@PathVariable String email, @PathVariable String password) throws Exception{

        User user = userDAO.getUserWithEmail(email);

        Boolean res = user != null && user.getPassword().equals(password);

        return ResponseEntity.ok().body(res);

    }
    @GetMapping("/check/{email}")
    public ResponseEntity<Boolean> checkEmail(@PathVariable String email) throws Exception{
        return ResponseEntity.ok().body(userService.paternMatches(email,userService.getRegexPatternForEmails()));
    }

    @GetMapping("/isAdmin")
    public ResponseEntity<Boolean> isAdmin(@RequestParam String email) throws Exception{

        User user = null;
        user = userDAO.getUserWithEmail(email);

        if(user==null)
            return ResponseEntity.ok().body(null);

        return ResponseEntity.ok().body(user.getEsAdmin()==1);

    }
}
