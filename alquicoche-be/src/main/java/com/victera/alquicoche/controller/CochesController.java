package com.victera.alquicoche.controller;

import com.victera.alquicoche.dao.CocheDAO;
import com.victera.alquicoche.model.Coche;
import com.victera.alquicoche.model.response.FiltersResponse;
import com.victera.alquicoche.model.response.ListCocheResponse;
import com.victera.alquicoche.model.response.Metadata;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class CochesController {

    @Autowired
    private CocheDAO cochesDAO;

    private String  oficinaNumberTOString(String oficina){
        String res = "";

        switch(oficina){
            case "1":
                res = "Madrid";
                break;
            case "2":
                res = "Toledo";
                break;
            case "3":
                res = "Valencia";
                break;
        }

        return res;
    }

    //Devuelve una lista de coches paginada y acorde a los diferentes filtros pedidos
    @GetMapping("/coches")
    public ResponseEntity<ListCocheResponse> coches(@Valid Metadata metadata, @Valid FiltersResponse filters) throws Exception{

        Boolean result = metadata.getPageNumber() > 0 && metadata.getPageSize() > 0;

        List<Coche> res = null;
        if (result) {
            // Cambia los numberos por oficinas
            filters.setOficinaReserva(oficinaNumberTOString(filters.getOficinaReserva()));
            filters.setOficinaDestino(oficinaNumberTOString(filters.getOficinaDestino()));

            res = cochesDAO.getPagesfromJoinOficeDate   (
                    metadata,
                    "coches",
                    filters
            );
            Integer totalRows = cochesDAO.numOfRows("coches",cochesDAO.joinOficinaFechaQuery(filters));
            double div = Math.ceil((double) totalRows / metadata.getPageSize());
            metadata.setTotalPages((int) div);
        }
        ListCocheResponse cocheResponse = new ListCocheResponse(res,metadata);

        result = result && metadata.getPageNumber()<= metadata.getTotalPages();
        
        return result
                ?ResponseEntity.ok().body(cocheResponse)
                :ResponseEntity.badRequest().build();

    }
    @GetMapping("/cocheMat")
    public ResponseEntity<Coche> getCocheMatricula(@RequestParam String matricula) throws Exception{

        Coche coche = cochesDAO.getCocheFromMatricula(matricula);
        return ResponseEntity.ok().body(coche);

    }

}
