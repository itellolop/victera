package com.victera.alquicoche.controller;

import com.victera.alquicoche.dao.CocheDAO;
import com.victera.alquicoche.dao.ReservaDAO;
import com.victera.alquicoche.dao.UserDAO;
import com.victera.alquicoche.model.Reserva;
import com.victera.alquicoche.model.response.ListReservaResponse;
import com.victera.alquicoche.model.response.Metadata;
import com.victera.alquicoche.model.response.ModifReservaResponse;
import com.victera.alquicoche.model.response.ReservaResponse;
import com.victera.alquicoche.service.PeriodoAnio;
import com.victera.alquicoche.service.PrecioReserva;
import com.victera.alquicoche.service.Mail;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.text.SimpleDateFormat;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class ReservasController {

    @Autowired
    ReservaDAO reservaDAO;

    @Autowired
    UserDAO userDAO;

    @Autowired
    CocheDAO cocheDAO;

    @Autowired
    PrecioReserva precioReserva;

    @Autowired
    PeriodoAnio periodoAnio;

    @Autowired
    Mail mail;

    @GetMapping("/reservasByUser")
    public ResponseEntity<ListReservaResponse> getUsers(@Valid Metadata metadata, @RequestParam String email) throws Exception{
        
        boolean result = metadata.getPageNumber() > 0 && metadata.getPageSize() > 0;
        List<Reserva> res = null;

        if(result){  
            res = reservaDAO.getReservaResponseFromUserId(metadata, email);
            Integer totalRows = reservaDAO.numOfRows("reserva", reservaDAO.joinIdUserReservaCoche(email));
            double div = Math.ceil((double) totalRows / metadata.getPageSize());
            metadata.setTotalPages((int) div);           
        }

        ListReservaResponse reservaResponse = new ListReservaResponse(res,metadata);

        result = result && metadata.getPageNumber() <= metadata.getTotalPages();

        return result
        ? ResponseEntity.ok().body(reservaResponse)
        : ResponseEntity.badRequest().build();

    }

    @GetMapping("/precioReserva")
    public ResponseEntity<Double> getPrecioReserva(@Valid Reserva reserva) throws Exception{
        return ResponseEntity.ok().body(precioReserva.calcular_precio_reserva(reserva));
    }

    @PostMapping("/insert")
    public Reserva insertReserva(@RequestBody ReservaResponse reservaResponse) throws Exception{

            String matricula = reservaResponse.getMatricula();
            String email = reservaResponse.getEmail();

            Reserva reservaFin = new Reserva(reservaResponse,matricula,email);

            //Como por defecto periodoReserva se inicializa como -1 en front, llamamos a getPeriodo para que nos devuelva el periodo correcto
            reservaFin.setPeriodoAnio(periodoAnio.getPeriodoAnio(reservaFin.getFechaReserva()));

            double costeReserva = precioReserva.calcular_precio_reserva(reservaFin);
            reservaFin.setCosteReserva(costeReserva);
            reservaDAO.insertReserva(reservaFin);

            //Hacer que el coche deje de estar disponible y cambiar su oficina por la oficina destino
            cocheDAO.updateDisponibleAndOficina(matricula,0,reservaResponse.getOficinaDestino());

            mail.sendMail(email, "Reserva realizada con éxito",
                    "<div><h1>Reserva realizada con éxito</h1><p>La reserva para el coche con matrícula "+ matricula +" ha sido registrada con éxito.<br>"+
                    "Podrá recoger su vehículo en "+reservaResponse.getOficinaReserva()+" a las "+ reservaFin.getHoraReserva() +" en la fecha "+
                     reservaFin.getFechaReserva() +".<br>Para más información, consulte nuestra página web.<br><br>"+
                    "Gracias por confiar en Alquicoche.<br><br>"+
                    "<img src=\"https://i.imgur.com/tQEMbH6.png\"></p></div>");
            return reservaFin;
    }

    @DeleteMapping("/deleteReserva/{idReserva}")
    public int deleteReserve(@PathVariable int idReserva) throws Exception{
        Reserva reserva = reservaDAO.getReservaFromIdReserva(idReserva);
        reservaDAO.deleteReserva(idReserva);
        mail.sendMail(reserva.getEmail(), "Reserva cancelada con éxito",
                    "<div><h1>Reserva cancelada con éxito</h1><p>Su reserva planificada en "+reserva.getOficinaReserva()+" a las "+ reserva.getHoraReserva() +" en la fecha "+
                    reserva.getFechaReserva() +"  fue cancelada con éxito.<br>"+
                    "Para más información, consulte la página web de Alquicoche.<br><br>"+
                    "Gracias por confiar en Alquicoche.<br><br>"+
                    "<img src=\"https://i.imgur.com/tQEMbH6.png\"></p></div>");
        
        return idReserva;
    }

    @PostMapping("/modificar/{idReserva}")
    public void modificarReserva(@PathVariable int idReserva, @RequestBody ModifReservaResponse modifReservaResponse) throws Exception{
        Reserva reserva = reservaDAO.getReservaFromIdReserva(idReserva);
        
        //combinar la fecha y hora para poder calcular correctamente la diferencia
        String fechaReserva = reserva.getFechaReserva().toString();
        String horaReserva = reserva.getHoraReserva().toString();
        String stringTiempo = fechaReserva + " " + horaReserva;
        java.util.Date dateReserva = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(stringTiempo);

        long now = System.currentTimeMillis();
        
        //cambiar los datos de la reserva si se ha solicitado su modificación
        if (modifReservaResponse.getMatricula()!=null)
            reserva.setMatricula(modifReservaResponse.getMatricula());
        
        if (modifReservaResponse.getTarjetaCredito()!=null)
            reserva.setTarjetaCredito(modifReservaResponse.getTarjetaCredito());

        if (modifReservaResponse.getTarifaCliente()!=-1)
            reserva.setTarifaCliente(modifReservaResponse.getTarifaCliente());

        if (modifReservaResponse.getExtrasCoche()!=-1)
            reserva.setExtrasCoche(modifReservaResponse.getExtrasCoche());

        double costeReserva = precioReserva.calcular_precio_reserva(reserva);

        if((now-dateReserva.getTime())/1000 >= 432000)
            reserva.setCosteReserva(costeReserva);
        if((now-dateReserva.getTime())/1000 < 432000 && (now-dateReserva.getTime())/1000 >= 86400)
            reserva.setCosteReserva(costeReserva+costeReserva/4);

        if((now-dateReserva.getTime())/1000 < 86400)
            reserva.setCosteReserva(costeReserva+costeReserva/2);


        //actualiza la reserva en la base de datos
        reservaDAO.actualizarReserva(reserva);
        mail.sendMail(reserva.getEmail(), "Reserva modificada con éxito",
                    "<div><h1>Reserva modificada con éxito</h1><p>Su reserva planificada en "+reserva.getOficinaReserva()+" a las "+ reserva.getHoraReserva() +" en la fecha "+
                    reserva.getFechaReserva() +"  ha sido modificada con éxito.<br>"+
                    "Para más información, consulte la página web de Alquicoche.<br><br>"+
                    "Gracias por confiar en Alquicoche.<br><br>"+
                    "<img src=\"https://i.imgur.com/tQEMbH6.png\"></p></div>");
    }
    
}