package com.victera.alquicoche.config;

import org.apache.commons.dbutils.QueryRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;



@Configuration
public class BeanConfiguration {

  @Bean
  public QueryRunner queryRunner() {
    return new QueryRunner();
  }

}
