CREATE TABLE bzkcsila4xkbpgj6bnhh.coches (
	nombreDoctor VARCHAR(70) NOT NULL
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_general_ci;

CREATE TABLE reserva(
	idReserva int NOT NULL AUTO_INCREMENT PRIMARY KEY,
	tarjetaCredito VARCHAR(16) NOT NULL,
    matricula VARCHAR(7) NOT NULL,
    email VARCHAR(60) NOT NULL,
    fechaReserva DATE NOT NULL,
    horaReserva TIME NOT NULL,
    oficinaReserva VARCHAR(40) NOT NULL,
    fechaDestino DATE NOT NULL,
    horaDestino TIME NOT NULL,
    oficinaDestino VARCHAR(40) NOT NULL, 
    estadoReserva int NOT NULL,
    costeReserva double NOT NULL,
    tarifaCliente int NOT NULL,
    tarifaCoche int NOT NULL,
    periodoAnio int NOT NULL,
    tipoCliente int NOT NULL,
    extrasCoche int
);


INSERT INTO reserva VALUES
	(1, '1477147714771477', '7144HDF', 'aaaa@gmail.com', CAST('2022-04-02' AS DATE), CAST('10:15' AS TIME), 'Toledo', CAST('2022-06-06' AS DATE), CAST('18:45' AS TIME), 'Madrid', 0, 150, 4, 1, 0, 1, 12),
    (2, '2345234523452345', '8721GJF', 'aaaa@gmail.com', CAST('2022-06-01' AS DATE), CAST('09:00' AS TIME), 'Toledo', CAST('2022-06-15' AS DATE), CAST('19:00' AS TIME), 'Madrid', 0, 300, 0, 1, 1, 0, 3),
    (3, '3456345634563456', '8696KJG', 'aaaa@gmail.com', CAST('2022-06-09' AS DATE), CAST('18:15' AS TIME), 'Valencia', CAST('2022-06-10' AS DATE), CAST('13:00' AS TIME), 'Madrid', 0, 40, 1, 2, 1, 1, 5),
    (4, '1234123412341234', '6267KJV', 'gpi@gmail.com', CAST('2022-06-10' AS DATE), CAST('15:45' AS TIME), 'Madrid', CAST('2022-06-15' AS DATE), CAST('20:00' AS TIME), 'Madrid', 0, 75, 0, 0, 1, 1, 4),
    (5, '5678567856785678', '8932JTJ', 'ejemplo@gmail.com', CAST('2022-06-17' AS DATE), CAST('10:00' AS TIME), 'Madrid', CAST('2022-06-19' AS DATE), CAST('17:30' AS TIME), 'Toledo', 0, 150, 3, 0, 1, 1, 8),
    (6, '6789678967896789', '3995FYH', 'ejemplo@gmail.com', CAST('2022-06-19' AS DATE), CAST('16:30' AS TIME), 'Madrid', CAST('2022-06-26' AS DATE), CAST('11:30' AS TIME), 'Valencia', 0, 500, 2, 0, 1, 0, 11),
    (7, '4567456745674567', '0383CDJ', 'walle@gmail.com', CAST('2022-06-29' AS DATE), CAST('09:30' AS TIME), 'Valencia', CAST('2022-08-27' AS DATE), CAST('19:30' AS TIME), 'Toledo', 0, 1250, 4, 2, 1, 0, 6),
    (8, '7890789078907890', '1387FJZ', 'walle@gmail.com', CAST('2022-07-10' AS DATE), CAST('16:00' AS TIME), 'Toledo', CAST('2022-07-17' AS DATE), CAST('18:00' AS TIME), 'Valencia', 0, 475, 2, 1, 1, 1, 14);
                   
create table coches (  
    modelo VARCHAR(60) NOT NULL,     
    marca VARCHAR(20) NOT NULL,    
    gama VARCHAR(10) NOT NULL,    
    oficina VARCHAR(40) NOT NULL,    
    cambio VARCHAR(15) NOT NULL,    
    puertas int NOT NULL,    
    techoSolar boolean NOT NULL,   
    disponible boolean NOT NULL,
    matricula VARCHAR(7) NOT NULL,
    PRIMARY KEY (matricula)
);

insert into coches values
	('Corolla', 'Toyota', 'media', 'Madrid', 'automatico', 5, false, true, '9142KNR'),
    ('Kona', 'Hyundai', 'media', 'Madrid', 'automatico', 5, false, true, '2520JRV'),
    ('Jimny', 'Suzuki', 'media', 'Madrid', 'manual', 3, false, false, '7144HDF'),
    ('500', 'Fiat', 'media', 'Madrid', 'automatico', 3, false, true, '0165JPL'),
    ('3008', 'Peugeot', 'media', 'Madrid', 'manual', 5, true, true, '5601DYP'),
    ('Cooper', 'Mini', 'media', 'Toledo', 'automatico', 3, true, true, '5185LCJ'),
    ('Clase B', 'Mercedes-Benz', 'media', 'Toledo', 'automatico', 5, false, false, '8721GJF'),
    ('Jazz', 'Honda', 'media', 'Toledo', 'automatico', 5, false, true, '1552CKL'),
    ('CX3', 'Mazda', 'media', 'Toledo', 'manual', 5, false, true, '5213LLG'),
    ('Q2', 'Audi', 'media', 'Toledo', 'automatico', 5, false, false, '3995FYH'),
    ('C1', 'Citroen', 'baja', 'Madrid', 'manual', 5, false, true, '6005GDD'),
    ('Panda', 'Fiat', 'baja', 'Madrid', 'manual', 5, false, true, '8309GBZ'),
    ('Fiesta', 'Ford', 'baja', 'Madrid', 'manual', 5, false, true, '4944JMT'),
    ('i20', 'Hyundai', 'baja', 'Madrid', 'manual', 5, false, false, '6267KJV'),
    ('208', 'Peugeot', 'baja', 'Madrid', 'manual', 5, false, true, '7683FBP'),
    ('Clio', 'Renault', 'baja', 'Toledo', 'manual', 5, false, false, '8932JTJ'),
    ('Sandero', 'Dacia', 'baja', 'Toledo', 'manual', 5, false, true, '2537FSR'),
    ('Aygo', 'Toyota', 'baja', 'Toledo', 'manual', 5, false, false,'1387FJZ'),
    ('Picanto', 'Kia', 'baja', 'Toledo', 'manual', 5, false, true, '2559HVL'),
    ('Space Star', 'Mitsubishi', 'baja', 'Toledo', 'manual', 5, false, true, '9933JLL'),
    ('Stelvio', 'Alfa Romeo', 'alta', 'Madrid', 'automatico', 5, false, false, '8696KJG'),
    ('Z4', 'BMW', 'alta', 'Madrid', 'automatico', 3, true, true, '3301BVD'),
	('RX', 'Lexus', 'alta', 'Madrid', 'automatico', 5, true, true, '0000BRJ'),
    ('Mustang', 'Ford', 'alta', 'Toledo', 'automatico', 3, false, true, '2727LGG'),
    ('XC60', 'Volvo', 'alta', 'Toledo', 'automatico', 5, false, true, '1041GQN'),
    ('Clase C', 'Mercedes-Benz', 'alta', 'Toledo', 'automatico', 5, true, false, '0383CDJ'),
    ('Corsa', 'Opel', 'baja', 'Valencia', 'manual', 5, false, true, '4436BTJ'),
    ('Rio', 'Kia', 'baja', 'Valencia', 'manual', 5, false, true, '0386LMJ'),
    ('i10', 'Hyundai', 'baja', 'Valencia', 'manual', 5, false, true, '6863LRL'),
    ('Logan', 'Dacia', 'baja', 'Valencia', 'manual', 5, false, true, '2811JBX'),
    ('Yaris', 'Toyota', 'media', 'Valencia', 'automatico', 5, false, true, '8894BYN'),
    ('Qashqai', 'Nissan', 'media', 'Valencia', 'manual', 5, false, true, '6542GBF'),
    ('Yaris', 'Toyota', 'media', 'Valencia', 'automatico', 5, false, true, '5327LDZ'),
    ('XV', 'Subaru', 'media', 'Valencia', 'automatico', 5, false, true, '9479FRG'),
    ('A3', 'Audi', 'media', 'Valencia', 'automatico', 5, false, true, '5427LFN'),
    ('Levante', 'Maserati', 'alta', 'Valencia', 'automatico', 5, false, true, '7528LNR'),
    ('F-Pace', 'Jaguar', 'alta', 'Valencia', 'automatico', 5, true, true, '4633FSZ'),
    ('Model 3', 'Tesla', 'alta', 'Valencia', 'automatico', 5, true, true, '9967JMW'),
    ('Multipla', 'Fiat', 'baja', 'Valencia', 'manual', 5, false, true, '9297FXQ');

create table users (
    email VARCHAR(60) NOT NULL,
    password VARCHAR(26) NOT NULL,
    nombre VARCHAR(20) NOT NULL,
    apellido VARCHAR(35) NOT NULL,
    dni VARCHAR(9) NOT NULL PRIMARY KEY,
    esAdmin int NOT NULL
    );

insert into users values
    ('aaaa@gmail.com', 'password','Antonio', 'Pino','12345678R', 0),
    ('bbbb@gmail.com', '1234','Meguru','Bachira','87654321S', 0),
    ('ejemplo@gmail.com', 'asdf','Sergio','Elprofe','12345678P', 0),
    ('tatsu@gmail.com', 'qwerty','Maestro','Shacolin','98765432L', 0),
    ('gpi@gmail.com', 'zxcv','The','Bausffs','66666666B', 0),
    ('walle@gmail.com', '9876','Thomas','Shellby','88888888U', 0),
    ('admin@gmail.com','oleole','Barry','Walter','74937457R', 1);